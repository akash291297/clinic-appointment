<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule_Weekday extends Model
{
    protected $table= "schedule_weekday";
    public $timestamps = false;

    public function weekday(){
    	return $this->belongsTo(Weekday::class,'weekday_id','id');
    }
}
