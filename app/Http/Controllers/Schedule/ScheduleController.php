<?php

namespace App\Http\Controllers\Schedule;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Schedule;
use App\Weekday;
use App\Doctor;
use App\Schedule_Weekday;
use Validator,Auth;

class ScheduleController extends Controller
{
    public function index(){
		$scheduleList = Schedule::with('schedule_weekday.weekday')->orderBy('id','desc')->paginate(10);
		if($scheduleList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $scheduleList;
		return Response()->json($scheduleList,200);
	}

	public function store(Request $request){
		$rules = [
			"doc_name"    => "required",
			"date"  => "required",
			"time"    => "required",
			"message"     => "required",
			"status"      => "required",
			"weekday"      => "required",
		];

		$errors = [
			"doc_name.required"  => "Enter The Doctor Name",
			"date.required"  => "Enter The date",
			"time.required"    => "Enter The End Time",
			"message.required"     => "Enter The Message",
			"status.required"      => "Give Your Status",
			"weekday.required"      => "Give Your Weekday",
 
		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		$ScheduleRegister = new Schedule;
		$ScheduleRegister->doc_name   = $request->doc_name;
		$ScheduleRegister->date       = $request->date;
		$ScheduleRegister->time       = $request->time;
		$ScheduleRegister->message    = $request->message;
		$ScheduleRegister->status     = $request->status;
		$ScheduleRegister->save();
	
		foreach ($request->weekday as $we) {
			
			$weekRegister = new Schedule_Weekday;
			$weekRegister->schedule_id = $ScheduleRegister->id;
			$weekRegister->weekday_id  = $we;
			$weekRegister->save(); 
		}

		$response['return'] = true;
		$response['message'] = 'Data inserted Successfully';
		$response['data'] = $ScheduleRegister;
		return Response()->json($ScheduleRegister,200); 
		
	}

	public function edit($id){
		$PatientEdit = Schedule::find($id);
		if($PatientEdit == null){
			$response['return'] = false;
			$response['message'] = 'Data Not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $PatientEdit;
		return Response()->json($PatientEdit,200); 
		// write $PatientEdit if rresponse not comes in json
	}

	public function update(Request $request){
		
		$rules = [
			"Patient_id" => "required|numeric",
			"doc_name"    => "required",
			"date"  => "required",
			"time"    => "required",
			"message"     => "required",
			"status"      => "required",
			"weekday"      => "required",
			
		];

		$errors = [
		"doc_name.required"  => "Enter The Doctor Name",
			"date.required"  => "Enter The date",
			"time.required"    => "Enter The End Time",
			"message.required"     => "Enter The Message",
			"status.required"      => "Give Your Status",
			"weekday.required"      => "Give Your Weekday",

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		$ScheduleRegister =  Schedule::find($request->Patient_id);
		$ScheduleRegister->doc_name   = $request->doc_name;
		$ScheduleRegister->date       = $request->date;
		$ScheduleRegister->time       = $request->time;
		$ScheduleRegister->message    = $request->message;
		$ScheduleRegister->status     = $request->status;
		$ScheduleRegister->save();

		Schedule_Weekday::where('schedule_id',$ScheduleRegister->id)->delete();
		foreach ($request->weekday as $we) {
			
			$weekRegister = new Schedule_Weekday;
			$weekRegister->schedule_id = $ScheduleRegister->id;
			$weekRegister->weekday_id  = $we;
			$weekRegister->save(); 
		}
		$response['return'] = true;
		$response['data'] = $ScheduleRegister;
		return Response()->json($response,200); 
	}

	public function destroy($id){
		$PatientDelete = Schedule::find($id);
		
		$PatientDelete->delete();
		$response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $PatientDelete;
        if($PatientDelete){
        	$newData = Schedule::orderBy('id','desc')->paginate(10);
        	$response['return'] = true;
        	$response['data']   = $newData;
        	return Response()->json($newData,200);
        }
        
       
	}

	public function weekday(){
			$weekday = Weekday::orderBy('id','asc')->get();
			if($weekday == null){
				$response['return']= false;
				$response['error'] = 'Data Not Found';
				return Response()->json($response,400);
			}
			$response['return'] = true;
			$response['data'] = $weekday;
			return Response()->json($weekday,200);
	}

	public function doctorname(){
		$doctorList = Doctor::orderBy('id','desc')->get();
		if($doctorList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $doctorList;
		return Response()->json($doctorList,200);
	}
}
