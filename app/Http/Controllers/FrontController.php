<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Front;
use App\Department;
use Validator;

class FrontController extends Controller
{


	public function index(){
		$departmentList = Front::orderBy('id','desc')->paginate(2);
		if($departmentList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data']   = $departmentList;
		return Response()->json($departmentList,200);
	}

     public function dept(){
        $departmentList = Department::orderBy('id','desc')->get();
        if($departmentList == null){
            $response['return'] = false;
            $response['message'] = 'Data not Found';
            return Response()->json($response,400);
        }

        $response['return'] = true;
        $response['data']   = $departmentList;
        return Response()->json($departmentList,200);
    }

    public function store(Request $request){

    	$rules = [

    		"name" => "required",
    		"email"=> "required|unique:front,email",
    		"mobile"=> "required|numeric",
    		"department"=> "required",
    		"message"=> "required",
    		"status" => "required"
    	];

    	$errors = [
    		"name.required" => "Enter name",
    		"email.required" => "Enter email",
    		"mobile.required" => "Enter mobile",
    		"department.required" => "Enter dept",
    		"message.required" => "Enter message",
    		"status.required" => "Enter status",

    	];

    	$Validator = Validator::make($request->all(),$rules,$errors);
    	if($Validator->fails()){
    		$keys = array_keys($Validator->getMessageBag()->toArray());
    		$response['errors_keys'] = $Validator->getMessageBag()->toArray();
    		$response['keys'] = $keys;
    		return Response()->json($response,400);
    	}

    	$frontRegister = new Front();
    	$frontRegister->name = $request->name;
    	$frontRegister->email = $request->email;
    	$frontRegister->mobile = $request->mobile;
    	$frontRegister->department = $request->department;
    	$frontRegister->message = $request->message;
    	$frontRegister->status = $request->status;
    	$frontRegister->save();
    	$response['return'] = true;
		$response['message'] = 'Data Inserted Successfully Admin Will Approve Status';
		$response['data']   = $frontRegister;
		return Response()->json($response,200); 
    }

    public function edit($id){
        $appointmentEdit = Front::find($id);
        if($appointmentEdit == null){
            $response['return'] = false;
            $response['message'] = 'Data Not Found';
            return Response()->json($response,400);
        }

        $response['return'] = true;
        $response['data'] = $appointmentEdit;
        return Response()->json($appointmentEdit,200); 
        // write $doctorEdit if rresponse not comes in json
    }


    public function update(Request $request){
        $rules = [
            "front_id" => "required|numeric",
            "name" => "required",
            "email"=> "required",
            "mobile"=> "required|numeric",
            "department"=> "required",
            "message"=> "required",
            "status" => "required"
        ];

        $errors = [
            "name.required" => "Enter name",
            "email.required" => "Enter email",
            "mobile.required" => "Enter mobile",
            "department.required" => "Enter dept",
            "message.required" => "Enter message",
            "status.required" => "Enter status",

        ];

        $Validator = Validator::make($request->all(),$rules,$errors);
        if($Validator->fails()){
            $keys = array_keys($Validator->getMessageBag()->toArray());
            $response['errors_keys'] = $Validator->getMessageBag()->toArray();
            $response['keys'] = $keys;
            return Response()->json($response,400);
        }

        $frontRegister =  Front::find($request->front_id);
        $frontRegister->name = $request->name;
        $frontRegister->email = $request->email;
        $frontRegister->mobile = $request->mobile;
        $frontRegister->department = $request->department;
        $frontRegister->message = $request->message;
        $frontRegister->status = $request->status;
        $frontRegister->save();
        $response['return'] = true;
        $response['message'] = 'Data Updated Successfully User Will Check Status';
        $response['data']   = $frontRegister;
        return Response()->json($response,200); 
    }

    public function destroy($id){
        $appointmentDelete = Front::find($id);
        $appointmentDelete->delete();
        $response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $appointmentDelete;
        if($appointmentDelete){
            $newData = Front::orderBy('id','desc')->paginate(10);
            $response['return'] = true;
            $response['data']   = $newData;
            return Response()->json($newData,200);
        }
        
    }
}
