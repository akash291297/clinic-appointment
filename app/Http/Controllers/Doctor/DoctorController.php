<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Doctor;
use Validator,Auth;

class DoctorController extends Controller
{
    public function index(){
		$doctorList = Doctor::orderBy('id','desc')->paginate(5);
		if($doctorList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $doctorList;
		return Response()->json($doctorList,200);
	}

	public function store(Request $request){
		$rules = [
			"name"   => "required",
			// "profession" => "required",
			"email"  => "required|email|unique:doctors,email",
			"phone"  => "required|min:10|numeric",
			"gender" => "required",
			
		];

		$errors = [
			"name.required"  => "Enter The Name",
			"email.required" => "Enter The Email",
			"phone.required" => "Enter The Phone No",

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		$doctorRegister = new Doctor();
		$doctorRegister->name   = $request->name;
		// $doctorRegister->profession   = $request->profession;
		$doctorRegister->email  = $request->email;
		$doctorRegister->phone  = $request->phone;
		$doctorRegister->gender = $request->gender;
		if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


       	
       		$doctorRegister->image = $name;
        }

       	$doctorRegister->save();
		$response['return'] = true;
		$response['message']= 'Data Inserted Successfully';
		$response['data'] = $doctorRegister;
		return Response()->json($response,200); 
		
	}

	public function edit($id){
		$doctorEdit = Doctor::find($id);
		if($doctorEdit == null){
			$response['return'] = false;
			$response['message'] = 'Data Not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $doctorEdit;
		return Response()->json($doctorEdit,200); 
		// write $doctorEdit if rresponse not comes in json
	}

	public function update(Request $request){
		
		$rules = [
			"doctor_id" => "required|numeric",
			"name"   => "required",
			// "profession" => "required",
			"email"  => "required|email",
			"phone"  => "required|min:10|numeric",
			"gender" => "required",
			
		];

		$errors = [
			"name.required"  => "Enter The Name",
			"email.required" => "Enter The Email",
			"phone.required" => "Enter The Phone No",

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		$doctorRegister =  Doctor::find($request->doctor_id);
		$doctorRegister->name   = $request->name;
		// $doctorRegister->profession   = $request->profession;
		$doctorRegister->email  = $request->email;
		$doctorRegister->phone  = $request->phone;
		$doctorRegister->gender = $request->gender;
		if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


       	
       		$doctorRegister->image = $name;
        }
		$doctorRegister->save();
		$response['return'] = true;
		$response['data'] = $doctorRegister;
		$response['message'] = "Updated Successfully";
		return Response()->json($response,200); 
	}

	public function destroy($id){
		$doctorDelete = Doctor::find($id);
		$doctorDelete->delete();
		$response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $doctorDelete;

        if($doctorDelete){
        	$newData = Doctor::orderBy('id','desc')->paginate(5);
        	$response['return'] = true;
        	$response['data']   = $newData;
        	return Response()->json($newData,200);
        }
	}
}
