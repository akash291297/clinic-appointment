<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Patient;
use Validator,Auth;

class PatientController extends Controller
{
    public function index(){
		$PatientList = Patient::orderBy('id','desc')->paginate(5);
		if($PatientList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $PatientList;
		return Response()->json($PatientList,200);
	}

	public function store(Request $request){
		$rules = [
			"name"   => "required",
			"email"  => "required|email|unique:patients,email",
			"phone"  => "required|min:10",
			"gender" => "required",
			
		];

		$errors = [
			"name.required"  => "Enter The Name",
			"email.required" => "Enter The Email",
			"phone.required" => "Enter The Phone No",

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		$PatientRegister = new Patient();
		$PatientRegister->name   = $request->name;
		$PatientRegister->email  = $request->email;
		$PatientRegister->phone  = $request->phone;
		$PatientRegister->gender = $request->gender;
		if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


       	
       		$PatientRegister->image = $name;
        }
		
		$PatientRegister->save();
		$response['return'] = true;
		$response['message'] = 'Data Inserted Successfully';
		$response['data'] = $PatientRegister;
		return Response()->json($response,200); 
		
	}

	public function edit($id){
		$PatientEdit = Patient::find($id);
		if($PatientEdit == null){
			$response['return'] = false;
			$response['message'] = 'Data Not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $PatientEdit;
		return Response()->json($PatientEdit,200); 
		// write $PatientEdit if rresponse not comes in json
	}

	public function update(Request $request){
		
		$rules = [
			"Patient_id" => "required|numeric",
			"name"   => "required",
			"email"  => "required|email",
			"phone"  => "required|min:10",
			"gender" => "required"
			
		];

		$errors = [
			"name.required"  => "Enter The Name",
			"email.required" => "Enter The Email",
			"phone.required" => "Enter The Phone No",

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		$PatientRegister =  Patient::find($request->Patient_id);
		$PatientRegister->name   = $request->name;
		$PatientRegister->email  = $request->email;
		$PatientRegister->phone  = $request->phone;
		$PatientRegister->gender = $request->gender;
		if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


       	
       		$PatientRegister->image = $name;
        }
		$PatientRegister->save();
		$response['return'] = true;
		$response['message'] = 'Data Inserted Successfully';
		$response['data'] = $PatientRegister;
		return Response()->json($PatientRegister,200); 
	}

	public function destroy($id){
		$PatientDelete = Patient::find($id);
		$PatientDelete->delete();
		$response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $PatientDelete;
         if($PatientDelete){
        	$newData = Patient::orderBy('id','desc')->paginate(10);
        	$response['return'] = true;
        	$response['data']   = $newData;
        	return Response()->json($newData,200);
        }
        
       
	}
}
