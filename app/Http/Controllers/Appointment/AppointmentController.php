<?php

namespace App\Http\Controllers\Appointment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Appointment;
use App\Patient;
use App\Department;
use App\Doctor;
use Validator,Auth;

class AppointmentController extends Controller
{
    public function index(){
		$appointmentList = Appointment::orderBy('id','desc')->paginate(10);
		if($appointmentList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data']   = $appointmentList;
		return Response()->json($appointmentList,200);
	}

	public function patient(){
		$patients = Patient::orderBy('id','desc')->get();
		if($patients == null){
			$response['return'] = false;
			$response['error'] = 'Data Not Found';
			return Response()->json($response,400);
		} 
		$response['return'] = true;
		$response['data'] = $patients;
		return Response()->json($patients,200);
	}

	public function department(){
		$departments = Department::orderBy('id','desc')->get();
		if($departments == null){
			$response['return'] = false;
			$response['error'] = 'Data Not Found';
			return Response()->json($response,400);
		}
		$response['return'] = true;
		$response['data'] = $departments;
		return Response()->json($departments,200);
	}

	public function doctor(){
		$doctor = Doctor::orderBy('id','desc')->get();
		if($doctor == null){
			$response['return'] = false;
			$response['error'] = 'Data Not Found';
			return Response()->json($response,400);
		}
		$response['return'] = true;
		$response['data'] = $doctor;
		return Response()->json($doctor,200);
	}

	public function store(Request $request){
		$rules = [
			"patient_name"  => "required",
			"department"   => "required",
			"doctor"       => "required",
			"date"         => "required",
			
			"message"      => "required",
			"status"       => "required",
			"patient_email"=> "required|email|unique:appointments,patient_email"
		];

		$errors = [
			"patient_name.required" => "Enter The patient Name",
			"department.required"   => "Enter The Department",
			"doctor.required"       => "Choose Your Doctor",
			"date.required"         => "Choose Your Date",
			"time.required"         => "Choose Your Time",
			"patient_email.required"=> "Patient Email"

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		$appointmentRegister = new Appointment();
		$appointmentRegister->patient_name = $request->patient_name;
		$appointmentRegister->department   = $request->department;
		$appointmentRegister->doctor       = $request->doctor;
		$appointmentRegister->date         = strtotime($request->date);
		// $appointmentRegister->time         = strtotime($request->time);
		$appointmentRegister->patient_email= $request->patient_email;
		$appointmentRegister->message      = $request->message;
		$appointmentRegister->status       = $request->status;
		$appointmentRegister->phone        = $request->phone;
		$appointmentRegister->save();
		$response['return'] = true;
		$response['message']= 'Data Inserted Successfully';
		$response['data']   = $appointmentRegister;
		return Response()->json($response,200); 
		
	}

	public function edit($id){
		$appointmentEdit = Appointment::find($id);
		if($appointmentEdit == null){
			$response['return'] = false;
			$response['message'] = 'Data Not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $appointmentEdit;
		return Response()->json($appointmentEdit,200); 
		// write $doctorEdit if rresponse not comes in json
	}

	public function update(Request $request){
		
		$rules = [
			"appointment_id" => "required|numeric",
			"patient_name"   => "required",
			"department"     => "required",
			"doctor"         => "required",
			"date"           => "required",
			"time"           => "required",
			"patient_email"  => "required|email|unique:appointments,patient_email"
		
		];

		$errors = [
			"patient_name.required" => "Enter The patient Name",
			"department.required"   => "Enter The Department",
			"doctor.required"       => "Choose Your Doctor",
			"date.required"         => "Choose Your Date",
			"time.required"         => "Choose Your Time",
			

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		
		$appointmentRegister =  Appointment::find($request->appointment_id);
		$appointmentRegister->patient_name = $request->patient_name;
		$appointmentRegister->department   = $request->department;
		$appointmentRegister->doctor       = $request->doctor;
		$appointmentRegister->date         = strtotime($request->date);
		$appointmentRegister->time         = strtotime($request->time);
		$appointmentRegister->patient_email= $request->patient_email;
		$appointmentRegister->message      = $request->message;
		$appointmentRegister->status       = $request->status;
		$appointmentRegister->phone        = $request->phone;
		$appointmentRegister->save();
		$response['return'] = true;
		$response['data']   = $appointmentRegister;
		return Response()->json($response,200); 
		
	}

	public function destroy($id){
		$appointmentDelete = Appointment::find($id);
		$appointmentDelete->delete();
		$response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $appointmentDelete;
        if($appointmentDelete){
        	$newData = Appointment::orderBy('id','desc')->paginate(10);
        	$response['return'] = true;
        	$response['data']   = $newData;
        	return Response()->json($newData,200);
        }
        
	}
}
