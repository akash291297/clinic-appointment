<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class LoginController extends Controller
{
    public function login(Request $request){

		$rules = [
			"email"     => "required|email",
			"password"  => "required"
		];

		$errors = [
			"email.required"    => "Enter Your Email Address",
			"password.required" => "Enter Your Passsword",
		];

		$validator = Validator::make($request->all(),$rules,$errors);

		if($validator->fails()){
    		$keys = array_keys($validator->getMessageBag()->toArray());
    		$response['error']      = $validator->getMessageBag()->toArray();
    		$response['error_keys'] = $keys;
    		return Response()->json($response,400);
    	}

		

       if(Auth::attempt($request->only('email','password'))){
		   
		    $response = Auth::user()->api_token;
			return response()->json($response,200);
       }

		else{
			$response['return'] = false;
			$response['error_message'] = 'Invalid Email or Password';
			return Response()->json($response,400);
		}
	}
}
