<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function  register(Request $request){

		$rules = [
					"name"     => "required",
					"email"	   => "required|unique:users,email|email",
					"password" => "required|max:10"
				];

	    $errors = [
	    			"name.required"     => "Enter Your Name",
	    			"email.required"    => "Enter Your Email id",
	    			"email.unique"      => "Email Should be Unique",
	    			"password.required" => "Password is required"
	    ];

	    $validator = Validator::make($request->all(),$rules,$errors);
	    if($validator->fails()){
	    	$keys = array_keys($validator->getMessageBag()->toArray());
	    	$response['errors'] = $validator->getMessageBag()->toArray();
	    	$response['error_keys'] =$keys;
	    	return Response()->json($response,400);
	    }	

	    $UserRegister = new User();
	    $UserRegister->name  = $request->name;
	    $UserRegister->email = $request->email;
	    $UserRegister->api_token = Str::random(10);
	    $UserRegister->password = bcrypt($request->password);
	    $UserRegister->save();
	    $response['return'] = true;
	    $response['message'] = 'Data inserted Successfully';
	    $response['data'] = $UserRegister;
	    return Response()->json($response,200);

	}
}
