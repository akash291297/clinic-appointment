<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Department;
use Validator,Auth;

class DepartmentController extends Controller
{
    public function index(){
		$departmentList = Department::orderBy('id','desc')->paginate(1);
		if($departmentList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data']   = $departmentList;
		return Response()->json($departmentList,200);
	}

	public function store(Request $request){
		$rules = [
			"name"         => "required",
			"description"  => "required",
			"status"       => "required"
		];

		$errors = [
			"name.required"        => "Enter The Name",
			"description.required" => "Enter The Description",
			"Status.required"      => "Choose Your Status",

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		$departmentRegister = new Department();
		$departmentRegister->name         = $request->name;
		$departmentRegister->description  = $request->description;
		$departmentRegister->status       = $request->status;
	    $departmentRegister->save();
		$response['return'] = true;
		$response['message'] = 'Data Inserted Successfully';
		$response['data']   = $departmentRegister;
		return Response()->json($response,200); 
		
	}

	public function edit($id){
		$departmentEdit = Department::find($id);
		if($departmentEdit == null){
			$response['return'] = false;
			$response['message'] = 'Data Not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $departmentEdit;
		return Response()->json($departmentEdit,200); 
		// write $doctorEdit if rresponse not comes in json
	}

	public function update(Request $request){
		
		$rules = [
			"department_id"=> "required|numeric", 
			"name"         => "required",
			"description"  => "required",
			"status"       => "required"
		];

		$errors = [
			"name.required"        => "Enter The Name",
			"description.required" => "Enter The Description",
			"Status.required"      => "Choose Your Status",

		];

		$Validator = Validator::make($request->all(),$rules,$errors);
		if($Validator->fails()){
			$keys = array_keys($Validator->getMessageBag()->toArray());
			$response['errors']     = $Validator->getMessageBag()->toArray();
			$response['error_keys'] = $keys;
			return Response()->json($response,400);
		}

		
		$departmentRegister = Department::find($request->department_id);
		$departmentRegister->name         = $request->name;
		$departmentRegister->description  = $request->description;
		$departmentRegister->status       = $request->status;
	    $departmentRegister->save();
		$response['return'] = true;
		$response['message'] = 'Data Updated Successfully';
		$response['data']   = $departmentRegister;
		return Response()->json($response,200); 
		
	}

	public function destroy($id){
		$departmentDelete = Department::find($id);
		$departmentDelete->delete();
		$response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $departmentDelete;
         if($departmentDelete){
        	$newData = Department::orderBy('id','desc')->paginate(10);
        	$response['return'] = true;
        	$response['data']   = $newData;
        	return Response()->json($newData,200);
        }
        
        
	}
}
