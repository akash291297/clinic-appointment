<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator,Auth;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    public function store(Request $request){

    	$rules=[
    		"current_pass" => "required",
    		"new_pass"     => "required|required_with:current_pass",
    		"repeat_pass"  => "same:new_pass"
    	];

    	$errors = [

    		"current_pass" => "Enter Your Current Password",
    		"new_pass"     => "Enter Your New Passsword",
    		"repeat_pass"  => "Repeat Your password"

    			  ];
    			  $user = Auth::user();
    	$validator = Validator::make($request->all(),$rules,$errors);
    	if($validator->fails()){
    		$keys = array_keys($validator->getMessageBag()->toArray());
    		$response['errors_keys'] = $validator->getMessageBag()->toArray();
    		$response['keys'] = $keys;
    		return Response()->json($response,400); 
    	}

    	if($request->get('current_pass')){
    		if(!(Hash::check($request->get('current_pass'),Auth::user()->password))){
    			$response['error'] = 'Your current password doesn match';
    			return Response()->json($response,400);
    		}
    	

    	if(strcmp($request->get('current_pass'),$request->get('new_pass'))==0){
    		$response['error'] = 'Your current password and new password cannot be same';
    		return Response()->json($response,400);
    	}

    	$user->password = bcrypt($request->new_pass);
    }
    	
    	$user->save();
    	$response['message'] = 'Password Changed';
    	return Response()->json($response,200);	
    }
}
