<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use App\Patient;
use App\Appointment;
use App\Department;

class CountController extends Controller
{
    
    public function doc(){
		$doctorList = Doctor::orderBy('id','desc')->get();
		if($doctorList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $doctorList;
		return Response()->json($doctorList,200);
	}


    public function pat(){
		$PatientList = Patient::orderBy('id','desc')->get();
		if($PatientList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data'] = $PatientList;
		return Response()->json($PatientList,200);
	}

	public function appint(){
		$appointmentList = Appointment::orderBy('id','desc')->get();
		if($appointmentList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data']   = $appointmentList;
		return Response()->json($appointmentList,200);
	}

	public function dept(){
		$departmentList = Department::orderBy('id','desc')->get();
		if($departmentList == null){
			$response['return'] = false;
			$response['message'] = 'Data not Found';
			return Response()->json($response,400);
		}

		$response['return'] = true;
		$response['data']   = $departmentList;
		return Response()->json($departmentList,200);
	}
}
