<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
	public $timestamps = false;
    protected $table   = 'schedule';
    
    public function schedule_weekday(){
	return $this->hasMany('App\Schedule_Weekday','schedule_id');
	}
}
