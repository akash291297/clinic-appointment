import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './components/HomeComponent.vue';
import AboutUsComponent from './components/AboutUsComponent.vue';
import ContactComponent from './components/ContactComponent.vue';
import DepartmentComponent from './components/DepartmentComponent.vue';
import DoctorComponent from './components/DoctorComponent.vue';
import ServiceComponent from './components/ServiceComponent.vue';
import AppointmentComponent from './components/AppointmentComponent.vue';
import LoginComponent from './components/LoginComponent.vue';
import RegisterComponent from './components/RegisterComponent.vue';
import DashboardComponent from './components/Admin/DashboardComponent.vue';
import BlankComponent from './components/Admin/BlankComponent.vue';
import SidebarComponent from './components/Admin/SidebarComponent.vue';
import DoctorIndex from './components/Admin/Doctor/IndexComponent.vue';
import DoctorCreate from './components/Admin/Doctor/CreateComponent.vue';
import DoctorEdit from './components/Admin/Doctor/EditComponent.vue';
import AppointmentIndex from './components/Admin/Appointment/IndexComponent.vue';
import AppointmentCreate from './components/Admin/Appointment/CreateComponent.vue';
import AppointmentEdit from './components/Admin/Appointment/EditComponent.vue';
import DepartmentIndex from './components/Admin/Departments/IndexComponent.vue';
import DepartmentCreate from './components/Admin/Departments/CreateComponent.vue';
import DepartmentEdit from './components/Admin/Departments/EditComponent.vue';
import ScheduleIndex from './components/Admin/DoctorSchedule/IndexComponent.vue';
import ScheduleCreate from './components/Admin/DoctorSchedule/CreateComponent.vue';
import ScheduleEdit from './components/Admin/DoctorSchedule/EditComponent.vue';
import PatientIndex from './components/Admin/Patients/IndexComponent.vue';
import PatientCreate from './components/Admin/Patients/CreateComponent.vue';
import PatientEdit from './components/Admin/Patients/EditComponent.vue';
import FrontIndex from './components/Admin/Front/IndexComponent.vue';
import FrontEdit from './components/Admin/Front/EditComponent.vue';
import Profilepass from './components/Admin/profile/IndexComponent.vue';



Vue.use(VueRouter)

const routes =[
    {
        path:'/',
        component:Home
    },
    {
        path:'/about',
        component:AboutUsComponent
    },
    {
        path:'/contact',
        component:ContactComponent
    },
    {
        path:'/department',
        component:DepartmentComponent
    },
    {
        path:'/sidebar',
        component:SidebarComponent
    },
    {
        path:'/doctor',
        component:DoctorComponent
    },
    {
        path:'/service',
        component:ServiceComponent
    },
    {
        path:'/appointment',
        component:AppointmentComponent
    },
    {
        path:'/login',
        component:LoginComponent,
        meta: { hideNavigation: true },

    },
    {
        path:'/register',
        component:RegisterComponent
    },
    {
        path:'/dashboard',
        component:DashboardComponent,
        meta:{hideNavigation:true},
        beforeEnter: (to, from, next) => {
        if(localStorage.getItem('token')){
            next();
        }else{
            next('/login');
        }
      }
    },
    {
        path:'/doctordetails',
        component:BlankComponent,
        children:[
            {
                path:'index',
                component:DoctorIndex
            },
            {
                path:'create',
                component:DoctorCreate
            },
            {
                path:'edit/:id',
                component:DoctorEdit,
                name:'editDoctor'
            }
        ]
    },
    {
        path:'/appointmentdetails',
        component:BlankComponent,
        children:[
            {
                path:'index',
                component:AppointmentIndex
            },
            {
                path:'create',
                component:AppointmentCreate
            },
            {
                path:'edit/:id',
                component:AppointmentEdit,
                name:'editAppoint'
            }
        ]
    },
    {
        path:'/frontdetails',
        component:BlankComponent,
        children:[
            {
                path:'index',
                component:FrontIndex
            },
            {
                path:'edit/:id',
                component:FrontEdit,
                name:'editFront'
            }
        ]
    },
    {
        path:'/departmentdetails',
        component:BlankComponent,
        children:[
            {
                path:'index',
                component:DepartmentIndex
            },
            {
                path:'create',
                component:DepartmentCreate
            },
            {
                path:'edit/:id',
                component:DepartmentEdit,
                name:'editDepart'
            }
        ]
    },
    {
        path:'/scheduledetails',
        component:BlankComponent,
        children:[
            {
                path:'index',
                component:ScheduleIndex
            },
            {
                path:'create',
                component:ScheduleCreate
            },
            {
                path:'edit/:id',
                component:ScheduleEdit,
                name:'editSchedule'
            }
        ]
    },
    {
        path:'/patientdetails',
        component:BlankComponent,
        children:[
            {
                path:'index',
                component:PatientIndex
            },
            {
                path:'create',
                component:PatientCreate
            },
            {
                path:'edit/:id',
                component:PatientEdit,
                name:'editCompany'
            }
        ]
    },
    {
        path:'/profile',
        component:BlankComponent,
        children:[
            {
              path:'password',
                component:Profilepass  
            }
            
        ]
    }
    
]

const router = new VueRouter({routes})

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem('token') || null  
  window.axios.defaults.headers['Authorization'] = 'Bearer '+token;
  next();
})
export default router