/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

Vue.component('pagination', require('laravel-vue-pagination'));
import router from './router';
import Navigation from './components/NavigationComponent.vue';
import Footing from './components/FooterComponent.vue'

new Vue({
    el: '#app',
    router,
    components:{
        
        'Navigation':Navigation,
        'Footing':Footing
    }
});
