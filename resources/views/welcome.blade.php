<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Medifab - Health &amp; Medical HTML Template</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.png')}}">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
		
		<!-- Owl Carousel Css -->
        <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
	    <!-- <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"> -->
		
        <!-- Admin -->
        <link rel="stylesheet" type="text/css" href="{{asset('admin/css/bootstrap.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/font-awesome.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/fullcalendar.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/dataTables.bootstrap4.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/select2.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/bootstrap-datetimepicker.min.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('admin/plugins/morris/morris.css')}}">
	    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/external.css')}}"> 
	    <!--   -->
	    
	    

    </head>    
    <body>
        <div id="app">
    		<Navigation v-if="!$route.meta.hideNavigation" /></Navigation>
        	<keep-alive>
				<router-view></router-view>
			</keep-alive>
			<Footing v-if="!$route.meta.hideNavigation"></Footing>
			
        </div>
    	<!-- Sidebar Overlay -->
        <div class="sidebar-overlay" data-reff="#side_menu"></div>
        <!-- App -->
		<script src="{{asset('js/app.js')}}"></script>
		
		<!-- jQuery -->
		<script src="{{asset('js/jquery.min.js')}}"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="{{asset('js/popper.min.js')}}"></script>
		<script src="{{asset('js/bootstrap.min.js')}}"></script>
		
		<!-- Owl Carousel JS -->
		<script src="{{asset('js/owl.carousel.min.js')}}"></script>
		
		<!-- Custom JS -->		
		<script src="{{asset('js/theme.js')}}"></script>

		<!-- Admin -->
		<script type="text/javascript" src="{{asset('admin/js/jquery-3.2.1.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('admin/js/popper.min.js')}}"></script>
	    <script type="text/javascript" src="{{asset('admin/js/bootstrap.min.js')}}"></script>
	    <script type="text/javascript" src="{{asset('admin/js/jquery.slimscroll.js')}}"></script>
	    <script type="text/javascript" src="{{asset('admin/js/slidemenu.html')}}"></script>
	    <script type="text/javascript" src="{{asset('admin/js/select2.min.js')}}"></script>
	    <script type="text/javascript" src="{{asset('admin/js/moment.min.js')}}"></script>
	    <script type="text/javascript" src="{{asset('admin/js/bootstrap-datetimepicker.min.js')}}"></script>
	    <script type="text/javascript" src="{{asset('admin/js/Chart.bundle.js')}}"></script>
	    <script type="text/javascript" src="{{asset('admin/js/app.js')}}"></script>
	</body>
</html>