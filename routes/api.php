<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Front Part Details
Route::post('/front-store','FrontController@store');
Route::get('/front-appoin','FrontController@dept');


Route::post('/login','Api\LoginController@login');
Route::post('/register','Api\RegisterController@register');
Route::group(['middleware' => ['auth:api']], function () {

    // Doctor Crud
    Route::get('/doctor-index','Doctor\DoctorController@index');
    Route::get('/doctor-create','Doctor\DoctorController@create');
    Route::post('/doctor-store','Doctor\DoctorController@store');
    Route::get('/doctor-edit/{id}','Doctor\DoctorController@edit');
    Route::post('/doctor-update','Doctor\DoctorController@update');
    Route::get('/doctor-delete/{id}','Doctor\DoctorController@destroy');

    // Patients Crud
    Route::get('/patient-index','Patient\PatientController@index');
    Route::get('/patient-create','Patient\PatientController@create');
    Route::post('/patient-store','Patient\PatientController@store');
    Route::get('/patient-edit/{id}','Patient\PatientController@edit');
    Route::post('/patient-update','Patient\PatientController@update');
    Route::get('/patient-delete/{id}','Patient\PatientController@destroy');

    // Appointments Crud
    Route::get('/appointment-index','Appointment\AppointmentController@index');
    Route::get('/appointment-create','Appointment\AppointmentController@create');
    Route::post('/appointment-store','Appointment\AppointmentController@store');
    Route::get('/appointment-edit/{id}','Appointment\AppointmentController@edit');
    Route::post('/appointment-update','Appointment\AppointmentController@update');
    Route::get('/appointment-delete/{id}','Appointment\AppointmentController@destroy');
    Route::get('/patient','Appointment\AppointmentController@patient');
    Route::get('/department','Appointment\AppointmentController@department');
    Route::get('/doctor','Appointment\AppointmentController@doctor');
    // DoctorSchedule
    Route::get('/schedule-index','Schedule\ScheduleController@index');
    Route::get('/schedule-create','Schedule\ScheduleController@create');
    Route::post('/schedule-store','Schedule\ScheduleController@store');
    Route::get('/schedule-edit/{id}','Schedule\ScheduleController@edit');
    Route::post('/schedule-update','Schedule\ScheduleController@update');
    Route::get('/schedule-delete/{id}','Schedule\ScheduleController@destroy');
    Route::get('/weekday','Schedule\ScheduleController@weekday');
    Route::get('/doctorname','Schedule\ScheduleController@doctorname');

    // Department
    Route::get('/department-index','Department\DepartmentController@index');
    Route::get('/department-create','Department\DepartmentController@create');
    Route::post('/department-store','Department\DepartmentController@store');
    Route::get('/department-edit/{id}','Department\DepartmentController@edit');
    Route::post('/department-update','Department\DepartmentController@update');
    Route::get('/department-delete/{id}','Department\DepartmentController@destroy');

    // Select Option Route
    Route::get('/patient','Appointment\AppointmentController@patient');
    Route::get('/department','Appointment\AppointmentController@department');
    Route::get('/doctor','Appointment\AppointmentController@doctor');

    // Front
    Route::get('/front-index','FrontController@index');
    Route::get('/front-edit/{id}','FrontController@edit');
    Route::post('/front-update','FrontController@update');
    Route::get('/front-delete/{id}','FrontController@destroy');
    Route::get('/front-dept','FrontController@dept');

    // Password
    Route::post('/password-store','PasswordController@store');

    // Count
    Route::get('/doctor-count','CountController@doc');
    Route::get('/patient-count','CountController@pat');
    Route::get('/appoint-count','CountController@appint');
    Route::get('/dept-count','CountController@dept');
    

});